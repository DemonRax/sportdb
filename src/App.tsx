import React from "react";
import { Link, Route, Switch } from "react-router-dom";
import { Empty } from "./Empty";
import { F1 } from "./F1";
import { Container, Row, Col } from "react-bootstrap";

export const App: React.FC = () => {
  return (
    <Container className="App" fluid>
      <Row>
        <Col>Header</Col>
      </Row>
      <Row>
        <Col xs="2">
          <nav className="navbar bg-light navbar-expand-sm navbar-light flex-column">
            <Link to="/" className="nav-link">
              Main
            </Link>
            <Link to="/f1" className="nav-link">
              F1
            </Link>
          </nav>
        </Col>
        <Col xs="10">
          <Switch>
            <Route exact path="/" component={Empty} />
            <Route path="/f1" component={F1} />
          </Switch>
        </Col>
      </Row>
      <Row>
        <Col>Footer</Col>
      </Row>
    </Container>
  );
};
