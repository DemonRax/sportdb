import { useEffect } from "react";
import { app } from "./Base";
import admin from "firebase";

type DocumentData = admin.firestore.DocumentData;
const dbF1 = app.firestore().collection("f1");

export const useFetchRaces = (setRaces: (d: DocumentData[]) => void): void => {
  useEffect(
    () =>
      dbF1.onSnapshot((snapshot) => {
        const temp: DocumentData[] = [];
        snapshot.forEach((doc) => {
          temp.push({ ...doc.data(), id: doc.id });
        });
        setRaces(temp);
      }),
    [setRaces]
  );
};
