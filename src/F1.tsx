import React, { useState } from "react";
import admin from "firebase";
import { useFetchRaces } from "./firebase/Firebase";

type DocumentData = admin.firestore.DocumentData;

export const F1: React.FC = () => {
  const [races, setRaces] = useState<DocumentData[]>([]);

  useFetchRaces(setRaces);

  return (
    <div>
      {races.map((v, i) => (
        <div key={i}>
          <span>{v.name}</span>
          <span>{v.ratings["AP"]}</span>
          <span>{v.ratings["RF"]}</span>
        </div>
      ))}
    </div>
  );
};
